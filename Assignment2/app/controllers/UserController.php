<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$password = $input['password'];
   		$encrypted = Hash::make($password);
   		$user = new User;
		$user->email = $input['email']; 
		$user->password = $encrypted; 
		$user->save();
		return "user created";
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function login()
	{
		$userdata = array(
		'username' => Input::get('email'),
		'firstName' => Input::get('firstName'),
		'lastName' => Input::get('lastName'),
		'password' => Input::get('password'),
		);
		
		//authenticate
		if (Auth::attempt($userdata)){
			return Redirect::to(URL::previous());
		} else {
			return Redirect::to(URL::previous())->withInput();
		}
	}
	public function logout()
	{
		Auth::logout();
		return Redirect::action('UserController@index');
	}
}

