<?php

class Post extends Eloquent {
    
function comments() {
      return $this->hasMany('Comment');
} 
function users() {
      return $this->hasMany('User');
} 
public static $rules = array( 
        'title' => 'required|min:4', 
        'text' => 'required',);
}