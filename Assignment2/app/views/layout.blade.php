<!-- Contains all the standardised elements of the pages -->
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Luke Butt">
<!-- Yield the title so each page is unique -->
    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="public/css/styles.css" rel="stylesheet">
    
     <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    
  </head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Not Facebook</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="public/docs/assessmentDetails.php">Documents </a></li>
              <li><a href="./">Photos </a></li>
              <li><a href="../navbar-static-top/">Friends</a></li>
              <li><a href="../navbar-fixed-top/">Login</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      <div class='row'>
        <!-- Area may be empty for this page -->
          <div class='col-sm-3'>
        @if (Auth::check())
            {{ Auth::user()->username }}
            {{ link_to_route('user.logout',"(Sign out)" )}}
        @else
            {{ Form::open(array('url' => secure_url('user/login'))) }}
            {{ Form::label('username', 'User Name: ') }}
            {{ Form::text('username') }}
            <!--{{ $errors->first('username') }}-->
            <p></p>
            {{ Form::label('password', 'Password: ') }} 
            {{ Form::password('password') }}
            <!--{{ $errors->first('password') }}-->
            <p></p>
            {{ Form::submit('Sign In') }} 
            {{ Form::close() }}
        @endif
          </div>
          <!--area to be filled with content  -->
          <div class='col-sm-9'>
                 @yield('content')
                  @show
          </div>
      </div>
      </div> <!-- /container -->
    
  </body>
</html>
@stop