@extends('layout')

@section('title')
Edit
@stop

@section('content')

    <h1>EDIT</h1>
    {{ Form::model($user, array('method' => 'PUT', 'route' => array('user.update', $user->id))) }}
    {{ Form::label('email', 'Username: ') }}
    {{ Form::text('email') }}
    {{ $errors->first('email') }}
    <p></p>
    {{ Form::label('password', 'Password: ') }} 
    {{ Form::text('password') }}
    {{ $errors->first('password') }}
    <p></p>
    {{ Form::submit('Update') }} 
    {{ Form::close() }}


@stop