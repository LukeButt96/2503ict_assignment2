
@extends('layout')

@section('title')
Create
@stop

@section('content')

    {{ Form::open(array('url' => secure_url('user'))) }}
    {{ Form::label('email', 'User Name: ') }}
    {{ Form::text('email') }}
    {{ $errors->first('username') }}
    <p></p>
    {{ Form::label('password', 'Password: ') }} 
    {{ Form::text('password') }}
    {{ $errors->first('password') }}
    <p></p>
    
    {{ Form::submit('Create') }} 
    {{ Form::close() }}


@stop