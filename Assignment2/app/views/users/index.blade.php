@extends('layout')

@section('title')
User
@stop

@section('content')
    <ul>
        @foreach ($users as $user)
        <li> {{ link_to_route('user.show', $user->firstName, $user->lastName, 
        $user->dateOfBirth, $user->dateOfBirth, array($user->id)) }} </li>
        @endforeach
    </ul>
    @foreach($posts as $post)
    <p>Title: {{{ $post->title }}}</p>
    <p>Text: {{{ $post->text }}}</p>
    <p>Name: {{{ $user->firstName }}}{{{ $user->lastName }}}</p>

    <p>{{ link_to_route('post.edit', 'Edit', array($post->id)) }} </p>
    
    ￼{{ Form::open(array('method' => 'DELETE', 'route' => array('post.destroy', $post->id))) }}
    
    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
    {{ Form::close() }}
    @endforeach
    @foreach($comments as $comment)
    <p>Text: {{{ $comment->text }}}</p>
    <p>Name: {{{ $user->firstName }}}{{{ $user->lastName }}}</p>

    <p>{{ link_to_route('comment.edit', 'Edit', array($comment->id)) }} </p>
    
    ￼{{ Form::open(array('method' => 'DELETE', 'route' => array('comment.destroy', $comment->id))) }}
    
    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
    {{ Form::close() }}
    @endforeach
    
@stop