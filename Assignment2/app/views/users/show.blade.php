@extends('layout')

@section('title')
Show
@stop

@section('content')

    <h1>User</h1>
    <p>First Name: {{{ $user->firstName }}}</p>
    <p>Last Name: {{{ $user->lastName }}}</p>
 
    <p>{{ link_to_route('user.edit', 'Edit', array($user->id)) }} </p>
    
    ￼{{ Form::open(array('method' => 'DELETE', 'route' => array('user.destroy', $user->id))) }}
    
    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
    {{ Form::close() }}

@stop