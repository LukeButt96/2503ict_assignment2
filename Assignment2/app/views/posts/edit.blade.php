@extends('layout')

@section('title')
Post Edit
@stop

@section('content')

    <h1>EDIT</h1>
    {{ Form::model($post, array('method' => 'PUT', 'route' => array('post.update', $post->id))) }}
    {{ Form::label('title', 'Title: ') }}
    {{ Form::text('title') }}
    {{ $errors->first('title') }}
    <p></p>
    {{ Form::label('text', 'Text: ') }} 
    {{ Form::text('text') }}
    {{ $errors->first('text') }}
    <p></p>
    {{ Form::submit('Update') }} 
    {{ Form::close() }}


@stop