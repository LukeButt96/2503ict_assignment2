@extends('layout')

@section('title')
Show
@stop

@section('content')

    <h1>Post</h1>
    <p>Title: {{{ $post->title }}}</p>
    <p>Text: {{{ $post->text }}}</p>
    <p>Name: {{{ $user->firstName }}}{{{ $user->lastName }}}</p>

    <p>{{ link_to_route('post.edit', 'Edit', array($post->id)) }} </p>
    
    ￼{{ Form::open(array('method' => 'DELETE', 'route' => array('post.destroy', $post->id))) }}
    
    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
    {{ Form::close() }}

@stop