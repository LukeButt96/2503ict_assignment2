
@extends('layout')

@section('title')
Post Create
@stop

@section('content')

    {{ Form::open(array('url' => secure_url('post'))) }}
    {{ Form::label('title', 'Title: ') }}
    {{ Form::text('title') }}
    {{ $errors->first('title') }}
    <p></p>
    {{ Form::label('text', 'Text: ') }} 
    {{ Form::text('text') }}
    {{ $errors->first('text') }}
    <p></p>
    
    {{ Form::submit('Create') }} 
    {{ Form::close() }}


@stop