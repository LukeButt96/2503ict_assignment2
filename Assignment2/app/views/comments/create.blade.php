
@extends('layout')

@section('title')
Comment Create
@stop

@section('content')

    {{ Form::open(array('url' => secure_url('comment'))) }}
    {{ Form::label('text', 'Text: ') }}
    {{ Form::text('text') }}
    {{ $errors->first('text') }}
    <p></p>
    
    {{ Form::submit('Create') }} 
    {{ Form::close() }}


@stop