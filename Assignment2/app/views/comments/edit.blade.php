@extends('layout')

@section('title')
Comment Edit
@stop

@section('content')

    <h1>EDIT</h1>
    {{ Form::model($comment, array('method' => 'PUT', 'route' => array('comment.update', $comment->id))) }}
    {{ Form::label('text', 'Text: ') }} 
    {{ Form::text('text') }}
    {{ $errors->first('text') }}
    <p></p>
    {{ Form::submit('Update') }} 
    {{ Form::close() }}


@stop