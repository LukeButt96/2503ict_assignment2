@extends('layout')

@section('title')
Show
@stop

@section('content')

    <h1>Post</h1>
    <p>Text: {{{ $comment->text }}}</p>
    <p>Name: {{{ $user->firstName }}}{{{ $user->lastName }}}</p>

    <p>{{ link_to_route('comment.edit', 'Edit', array($comment->id)) }} </p>
    
    ￼{{ Form::open(array('method' => 'DELETE', 'route' => array('comment.destroy', $comment->id))) }}
    
    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
    {{ Form::close() }}

@stop