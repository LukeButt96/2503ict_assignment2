<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::post('user/login', array('as' => 'user.login', 'users' => 'UserController@login')); 
Route::get('user/logout', array('as' => 'user.logout', 'users' => 'UserController@logout'));
Route::resource('user', 'UserController');
Route::resource('comment', 'CommentController');
Route::resource('post', 'PostController');

Route::get('/', array('as'=>'home', 'uses'=> 'UserController@index'));


