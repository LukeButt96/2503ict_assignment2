<?php

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// 1 
		$user = new User;
		$user->email = 'john@email.com';
		$user->password = 'john1234' ;
		$user->firstName = 'John' ;
		$user->lastName = 'Chaser' ;
		$user->dateOfBirth = '15/09/87' ;
		$user->save() ;
		// 2
		$user = new User;
		$user->email = 'bob@email.com';
		$user->password = 'bob1234' ;
		$user->firstName = 'Bob' ;
		$user->lastName = 'Jones' ;
		$user->dateOfBirth = '14/12/76' ;
		$user->save() ;
		// 3
		$user = new User;
		$user->email = 'jane@email.com';
		$user->password = 'jane1234' ;
		$user->firstName = 'Jane' ;
		$user->lastName = 'Trix' ;
		$user->dateOfBirth = '08/07/98' ;
		$user->save() ;
		// 4
		$user = new User;
		$user->email = 'Max@email.com';
		$user->password = 'max1234' ;
		$user->firstName = 'Max' ;
		$user->lastName = 'Million' ;
		$user->dateOfBirth = '22/03/79' ;
		$user->save() ;
		// 5
		$user = new User;
		$user->email = 'allen@email.com';
		$user->password = 'allen1234' ;
		$user->firstName = 'Allen' ;
		$user->lastName = 'Storm' ;
		$user->dateOfBirth = '29/04/90' ;
		$user->save() ;
		// 6
		$user = new User;
		$user->email = 'hurley@email.com';
		$user->password = 'hurley1234' ;
		$user->firstName = 'Hurley' ;
		$user->lastName = 'Guts' ;
		$user->dateOfBirth = '23/08/97' ;
		$user->save() ;
		// 7
		$user = new User;
		$user->email = 'linda@email.com';
		$user->password = 'linda1234' ;
		$user->firstName = 'Linda' ;
		$user->lastName = 'Hamilton' ;
		$user->dateOfBirth = '18/05/82' ;
		$user->save() ;
		// 8
		$user = new User;
		$user->email = 'carlie@email.com';
		$user->password = 'carlie1234' ;
		$user->firstName = 'Carlie' ;
		$user->lastName = 'Guns' ;
		$user->dateOfBirth = '28/12/01' ;
		$user->save() ;
		// 9
		$user = new User;
		$user->email = 'joe@email.com';
		$user->password = 'joe1234' ;
		$user->firstName = 'Joe' ;
		$user->lastName = 'Hobbs' ;
		$user->dateOfBirth = '01/01/82' ;
		$user->save() ;
		// 10
		$user = new User;
		$user->email = 'kasey@email.com';
		$user->password = 'kasey1234' ;
		$user->firstName = 'Kasey' ;
		$user->lastName = 'Grace' ;
		$user->dateOfBirth = '15/06/55' ;
		$user->save() ;
	
	}

}
