<?php

class CommentTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$comment = new Comment;
        $comment->text = 'Text for Comment 1 ';
        $comment->user_id = 1;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 2 ';
        $comment->user_id = 1;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 3 ';
        $comment->user_id = 1;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 4 ';
        $comment->user_id = 1;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 5 ';
        $comment->user_id = 1;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 6 ';
        $comment->user_id = 1;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 7 ';
        $comment->user_id = 1;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 8 ';
        $comment->user_id = 1;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 9 ';
        $comment->user_id = 1;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 10 ';
        $comment->user_id = 1;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 1 ';
        $comment->user_id = 2;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 2 ';
        $comment->user_id = 1;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 3 ';
        $comment->user_id = 1;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 1 ';
        $comment->user_id = 9;
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->text = 'Text for Comment 1 ';
        $comment->user_id = 10;
        $comment->post_id = 1;
        $comment->save();
        
	}

}
